# .bashrc

# Source global definitions
if [ -f /etc/bashrc ]; then
  . /etc/bashrc
fi

function return_code() {
  RC="$?"
  RED="\033[1;31m"
  GREEN="\e[32;1m"
  OFF="\033[m"
  if [ "${RC}" -eq 0 ]; then
    echo -e "${GREEN}${RC}${OFF}"
  else
    echo -e "${RED}${RC}${OFF}"
  fi
}

function update_dotfiles() {
  cd ~
  if [ ! -d dotfiles ]; then
    git clone https://github.com/kmmiles/dotfiles.git
    cd dotfiles
  else
    cd dotfiles
    git pull 
  fi

  chmod +x copy.sh
  ./copy.sh
  . ~/.bashrc
}

GIT_EDITOR=vim
VISUAL=vim
EDITOR=vim
PS1='\[\e]0;\w\a\]\n\[\e[32m\]\u@\h: \[\e[33m\]\w\[\e[0m\] [`return_code`]\n\$ '

MYDIR_COLORS="ansi-light"
_MYDIR_COLORS="$HOME/.dircolors.$MYDIR_COLORS"
[ -f "$_MYDIR_COLORS" ] && \
  eval $(dircolors -b "$_MYDIR_COLORS")

alias ls='ls --color'
alias less='less -R'
alias services="systemctl list-units --type service --all"

export PS1 GIT_EDITOR VISUAL EDITOR

################################################################################

# source tools
source "$HOME/.bash_tools"
if [ "$OS" == "Windows_NT" ]; then
  source "$HOME/.bash_windows"
fi

# source docker aliases
source "$HOME/.docker_aliases"
