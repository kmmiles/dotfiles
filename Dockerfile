FROM debian:stretch-slim

RUN set -eux; \
    apt-get update && \
    apt-get install -y \
      bash \
      bc \
      vim \
      screen \
      tmux \
      git

RUN groupadd -g 999 docker && \
    useradd -r -u 999 -g docker docker && \
    mkdir -p /home/docker && \
    chown -R docker:docker /home/docker && \
    echo "docker:docker" | chpasswd && \
    adduser docker sudo && \
    echo '%sudo ALL=(ALL) NOPASSWD:ALL' >> /etc/sudoers

USER docker

ADD . /home/docker/dotfiles
WORKDIR /home/docker
RUN dotfiles/bin/install

ENTRYPOINT ["/bin/bash"]
